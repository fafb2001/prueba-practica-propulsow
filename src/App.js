import './App.css';
import logo from './assets/images/logo.png';
import slider from './assets/images/slider.png'
import product from './assets/images/producto.png'
import especialistas1 from './assets/images/especialistas-1.png'
import especialistas2 from './assets/images/especialistas-2.png'
import especialistas3 from './assets/images/especialistas-3.png'
import especialistas4 from './assets/images/especialistas-4.png'



function App() {
  return (
    <div>
    <div className="info">Mensaje Informativo - Comercial</div>
    <header>
        <div className="container">
            <nav className ="nav-bar" >
                <img src={logo} alt="Logo"/>
                <ul className = "nav-font">
                    <li className='font-off'><a href="#">Nosotros</a></li>
                    <li className='font-off'><a href="#">Galeria</a></li>
                    <li className='font-off'><a href="#">Servicios</a></li>
                    <li className='font-off'><a href="#">Productos</a></li>
                    <li className='font-off'><a href="#">Contacto</a></li>
                    <li><a href="#"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M13.125 24.375C19.3382 24.375 24.375 19.3382 24.375 13.125C24.375 6.9118 19.3382 1.875 13.125 1.875C6.9118 1.875 1.875 6.9118 1.875 13.125C1.875 19.3382 6.9118 24.375 13.125 24.375Z" stroke="#1D0D85" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M21.5625 21.5625L28.125 28.125" stroke="#1D0D85" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
</a></li>
                    <li><a href="#"><svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M25.3333 8.00004H22.6667C22.6667 4.26671 19.7333 1.33337 16 1.33337C12.2667 1.33337 9.33333 4.26671 9.33333 8.00004H6.66667C5.2 8.00004 4 9.20004 4 10.6667V26.6667C4 28.1334 5.2 29.3334 6.66667 29.3334H25.3333C26.8 29.3334 28 28.1334 28 26.6667V10.6667C28 9.20004 26.8 8.00004 25.3333 8.00004ZM16 4.00004C18.2667 4.00004 20 5.73337 20 8.00004H12C12 5.73337 13.7333 4.00004 16 4.00004ZM25.3333 26.6667H6.66667V10.6667H25.3333V26.6667ZM16 16C13.7333 16 12 14.2667 12 12H9.33333C9.33333 15.7334 12.2667 18.6667 16 18.6667C19.7333 18.6667 22.6667 15.7334 22.6667 12H20C20 14.2667 18.2667 16 16 16Z" fill="#1D0D85"/>
</svg>
</a></li> <li className='vector-on'><svg width="22" height="18" viewBox="0 0 22 18" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M0.288086 10.5H21.2881V7.5H0.288086V10.5ZM0.288086 18H21.2881V15H0.288086V18ZM0.288086 0V3H21.2881V0H0.288086Z" fill="#1D0D85"/>
</svg>
</li>
                </ul>
            </nav>

            <div className="container-slider">
                <a href="#">
                <img className ='slider' src= {slider} alt="slider"/>
                </a>
            </div>
        </div>
    </header>

    <section>
        <div className="container-productos">
            <div className='card'>
                <div className='card-img'>
                    <img className='imagen' src={product} alt="producto"/>
                        <div className='banderin'>
                            <span className='new'>new</span>
                            <svg className='vector' width="114" height="95" viewBox="0 0 114 95" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 94.5581V0H113.675L0 94.5581Z" fill="#5943E3"/>
                                </svg>
                        </div>
                        <span className='discount'>20%</span>
                        <svg className='rectangle' width="42" height="40" viewBox="0 0 42 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="42" height="40" rx="20" fill="#D12727"/>
                            </svg>

                    </div>
                <div className='card-text'>
                    <div className='card-info'>
                        <p className='descripcion'>Nombre de producto ejemplo largo</p>
                        <p>
                            <span className='og-precio'>$19.990 </span>
                            <span className='precio'>$9.990</span>
                            </p>
                            <a className='buy-btn' href="#"> <svg width="25" height="25" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M25.4298 6.33569C24.9368 5.73342 24.1701 5.36737 23.3767 5.36737H6.70655L6.18688 3.43074C5.88533 2.30495 4.846 1.51965 3.64089 1.51965H0.849029C0.383892 1.51965 0 1.8857 0 2.33154C0 2.77633 0.382797 3.14343 0.849029 3.14343H3.64089C4.0515 3.14343 4.40758 3.40519 4.51664 3.79784L7.85598 16.5201C8.15753 17.6459 9.19686 18.4312 10.402 18.4312H21.3236C22.5276 18.4312 23.5959 17.6459 23.8696 16.5201L25.9226 8.4575C26.114 7.72333 25.9504 6.93803 25.4297 6.33577L25.4298 6.33569ZM24.2535 8.0893L22.2005 16.1519C22.0914 16.5445 21.7353 16.8063 21.3247 16.8063H10.402C9.99139 16.8063 9.63531 16.5445 9.52625 16.1519L7.14494 7.01685H23.3779C23.6517 7.01685 23.9254 7.14773 24.0901 7.35735C24.2537 7.56595 24.335 7.82772 24.2537 8.08948L24.2535 8.0893Z" fill="white"/>
                                <path d="M10.9777 19.476C9.41759 19.476 8.13135 20.706 8.13135 22.1978C8.13135 23.6895 9.4177 24.9196 10.9777 24.9196C12.5378 24.9206 13.824 23.6906 13.824 22.1986C13.824 20.7066 12.5377 19.4757 10.9777 19.4757V19.476ZM10.9777 23.2715C10.3479 23.2715 9.85492 22.8002 9.85492 22.1979C9.85492 21.5956 10.3479 21.1242 10.9777 21.1242C11.6075 21.1242 12.1005 21.5956 12.1005 22.1979C12.0994 22.7746 11.5797 23.2715 10.9777 23.2715Z" fill="white"/>
                                <path d="M20.3938 19.476C18.8337 19.476 17.5475 20.706 17.5475 22.1978C17.5475 23.6895 18.8338 24.9196 20.3938 24.9196C21.9538 24.9196 23.2402 23.6895 23.2402 22.1978C23.2134 20.7069 21.9538 19.476 20.3938 19.476ZM20.3938 23.2715C19.764 23.2715 19.2711 22.8001 19.2711 22.1979C19.2711 21.5956 19.764 21.1242 20.3938 21.1242C21.0236 21.1242 21.5166 21.5956 21.5166 22.1979C21.5166 22.7746 20.9959 23.2715 20.3938 23.2715Z" fill="white"/>
                                </svg> Comprar</a>
                        </div>
                    
                </div>
            </div>
            <div className='card tarjeta2'>
                <div className='card-img'>
                    <img className='imagen' src={product} alt="producto"/>
                        <div className='banderin'>
                            <span className='new'>new</span>
                            <svg className='vector' width="114" height="95" viewBox="0 0 114 95" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 94.5581V0H113.675L0 94.5581Z" fill="#5943E3"/>
                                </svg>
                        </div>
                        <span className='discount'>20%</span>
                        <svg className='rectangle' width="42" height="40" viewBox="0 0 42 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="42" height="40" rx="20" fill="#D12727"/>
                            </svg>

                    </div>
                <div className='card-text'>
                    <div className='card-info'>
                        <p className='descripcion'>Nombre de producto ejemplo largo</p>
                        <p>
                            <span className='og-precio'>$19.990 </span>
                            <span className='precio'>$9.990</span>
                            </p>
                            <a className='buy-btn' href="#"> <svg width="25" height="25" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M25.4298 6.33569C24.9368 5.73342 24.1701 5.36737 23.3767 5.36737H6.70655L6.18688 3.43074C5.88533 2.30495 4.846 1.51965 3.64089 1.51965H0.849029C0.383892 1.51965 0 1.8857 0 2.33154C0 2.77633 0.382797 3.14343 0.849029 3.14343H3.64089C4.0515 3.14343 4.40758 3.40519 4.51664 3.79784L7.85598 16.5201C8.15753 17.6459 9.19686 18.4312 10.402 18.4312H21.3236C22.5276 18.4312 23.5959 17.6459 23.8696 16.5201L25.9226 8.4575C26.114 7.72333 25.9504 6.93803 25.4297 6.33577L25.4298 6.33569ZM24.2535 8.0893L22.2005 16.1519C22.0914 16.5445 21.7353 16.8063 21.3247 16.8063H10.402C9.99139 16.8063 9.63531 16.5445 9.52625 16.1519L7.14494 7.01685H23.3779C23.6517 7.01685 23.9254 7.14773 24.0901 7.35735C24.2537 7.56595 24.335 7.82772 24.2537 8.08948L24.2535 8.0893Z" fill="white"/>
                                <path d="M10.9777 19.476C9.41759 19.476 8.13135 20.706 8.13135 22.1978C8.13135 23.6895 9.4177 24.9196 10.9777 24.9196C12.5378 24.9206 13.824 23.6906 13.824 22.1986C13.824 20.7066 12.5377 19.4757 10.9777 19.4757V19.476ZM10.9777 23.2715C10.3479 23.2715 9.85492 22.8002 9.85492 22.1979C9.85492 21.5956 10.3479 21.1242 10.9777 21.1242C11.6075 21.1242 12.1005 21.5956 12.1005 22.1979C12.0994 22.7746 11.5797 23.2715 10.9777 23.2715Z" fill="white"/>
                                <path d="M20.3938 19.476C18.8337 19.476 17.5475 20.706 17.5475 22.1978C17.5475 23.6895 18.8338 24.9196 20.3938 24.9196C21.9538 24.9196 23.2402 23.6895 23.2402 22.1978C23.2134 20.7069 21.9538 19.476 20.3938 19.476ZM20.3938 23.2715C19.764 23.2715 19.2711 22.8001 19.2711 22.1979C19.2711 21.5956 19.764 21.1242 20.3938 21.1242C21.0236 21.1242 21.5166 21.5956 21.5166 22.1979C21.5166 22.7746 20.9959 23.2715 20.3938 23.2715Z" fill="white"/>
                                </svg> Comprar</a>
                        </div>
                    
                </div>
            </div>
            <div className='card tarjeta3'>
                <div className='card-img'>
                    <img className='imagen' src={product} alt="producto"/>
                        <div className='banderin'>
                            <span className='new'>new</span>
                            <svg className='vector' width="114" height="95" viewBox="0 0 114 95" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 94.5581V0H113.675L0 94.5581Z" fill="#5943E3"/>
                                </svg>
                        </div>
                        <span className='discount'>20%</span>
                        <svg className='rectangle' width="42" height="40" viewBox="0 0 42 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="42" height="40" rx="20" fill="#D12727"/>
                            </svg>

                    </div>
                <div className='card-text'>
                    <div className='card-info'>
                        <p className='descripcion'>Nombre de producto ejemplo largo</p>
                        <p>
                            <span className='og-precio'>$19.990 </span>
                            <span className='precio'>$9.990</span>
                            </p>
                            <a className='buy-btn' href="#"> <svg width="25" height="25" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M25.4298 6.33569C24.9368 5.73342 24.1701 5.36737 23.3767 5.36737H6.70655L6.18688 3.43074C5.88533 2.30495 4.846 1.51965 3.64089 1.51965H0.849029C0.383892 1.51965 0 1.8857 0 2.33154C0 2.77633 0.382797 3.14343 0.849029 3.14343H3.64089C4.0515 3.14343 4.40758 3.40519 4.51664 3.79784L7.85598 16.5201C8.15753 17.6459 9.19686 18.4312 10.402 18.4312H21.3236C22.5276 18.4312 23.5959 17.6459 23.8696 16.5201L25.9226 8.4575C26.114 7.72333 25.9504 6.93803 25.4297 6.33577L25.4298 6.33569ZM24.2535 8.0893L22.2005 16.1519C22.0914 16.5445 21.7353 16.8063 21.3247 16.8063H10.402C9.99139 16.8063 9.63531 16.5445 9.52625 16.1519L7.14494 7.01685H23.3779C23.6517 7.01685 23.9254 7.14773 24.0901 7.35735C24.2537 7.56595 24.335 7.82772 24.2537 8.08948L24.2535 8.0893Z" fill="white"/>
                                <path d="M10.9777 19.476C9.41759 19.476 8.13135 20.706 8.13135 22.1978C8.13135 23.6895 9.4177 24.9196 10.9777 24.9196C12.5378 24.9206 13.824 23.6906 13.824 22.1986C13.824 20.7066 12.5377 19.4757 10.9777 19.4757V19.476ZM10.9777 23.2715C10.3479 23.2715 9.85492 22.8002 9.85492 22.1979C9.85492 21.5956 10.3479 21.1242 10.9777 21.1242C11.6075 21.1242 12.1005 21.5956 12.1005 22.1979C12.0994 22.7746 11.5797 23.2715 10.9777 23.2715Z" fill="white"/>
                                <path d="M20.3938 19.476C18.8337 19.476 17.5475 20.706 17.5475 22.1978C17.5475 23.6895 18.8338 24.9196 20.3938 24.9196C21.9538 24.9196 23.2402 23.6895 23.2402 22.1978C23.2134 20.7069 21.9538 19.476 20.3938 19.476ZM20.3938 23.2715C19.764 23.2715 19.2711 22.8001 19.2711 22.1979C19.2711 21.5956 19.764 21.1242 20.3938 21.1242C21.0236 21.1242 21.5166 21.5956 21.5166 22.1979C21.5166 22.7746 20.9959 23.2715 20.3938 23.2715Z" fill="white"/>
                                </svg> Comprar</a>
                        </div>
                    
                </div>
            </div>
            <div className='card tarjeta4'>
                <div className='card-img'>
                    <img className='imagen' src={product} alt="producto"/>
                        <div className='banderin'>
                            <span className='new'>new</span>
                            <svg className='vector' width="114" height="95" viewBox="0 0 114 95" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 94.5581V0H113.675L0 94.5581Z" fill="#5943E3"/>
                                </svg>
                        </div>
                        <span className='discount'>20%</span>
                        <svg className='rectangle' width="42" height="40" viewBox="0 0 42 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="42" height="40" rx="20" fill="#D12727"/>
                            </svg>

                    </div>
                <div className='card-text'>
                    <div className='card-info'>
                        <p className='descripcion'>Nombre de producto ejemplo largo</p>
                        <p>
                            <span className='og-precio'>$19.990 </span>
                            <span className='precio'>$9.990</span>
                            </p>
                            <a className='buy-btn' href="#"> <svg width="25" height="25" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M25.4298 6.33569C24.9368 5.73342 24.1701 5.36737 23.3767 5.36737H6.70655L6.18688 3.43074C5.88533 2.30495 4.846 1.51965 3.64089 1.51965H0.849029C0.383892 1.51965 0 1.8857 0 2.33154C0 2.77633 0.382797 3.14343 0.849029 3.14343H3.64089C4.0515 3.14343 4.40758 3.40519 4.51664 3.79784L7.85598 16.5201C8.15753 17.6459 9.19686 18.4312 10.402 18.4312H21.3236C22.5276 18.4312 23.5959 17.6459 23.8696 16.5201L25.9226 8.4575C26.114 7.72333 25.9504 6.93803 25.4297 6.33577L25.4298 6.33569ZM24.2535 8.0893L22.2005 16.1519C22.0914 16.5445 21.7353 16.8063 21.3247 16.8063H10.402C9.99139 16.8063 9.63531 16.5445 9.52625 16.1519L7.14494 7.01685H23.3779C23.6517 7.01685 23.9254 7.14773 24.0901 7.35735C24.2537 7.56595 24.335 7.82772 24.2537 8.08948L24.2535 8.0893Z" fill="white"/>
                                <path d="M10.9777 19.476C9.41759 19.476 8.13135 20.706 8.13135 22.1978C8.13135 23.6895 9.4177 24.9196 10.9777 24.9196C12.5378 24.9206 13.824 23.6906 13.824 22.1986C13.824 20.7066 12.5377 19.4757 10.9777 19.4757V19.476ZM10.9777 23.2715C10.3479 23.2715 9.85492 22.8002 9.85492 22.1979C9.85492 21.5956 10.3479 21.1242 10.9777 21.1242C11.6075 21.1242 12.1005 21.5956 12.1005 22.1979C12.0994 22.7746 11.5797 23.2715 10.9777 23.2715Z" fill="white"/>
                                <path d="M20.3938 19.476C18.8337 19.476 17.5475 20.706 17.5475 22.1978C17.5475 23.6895 18.8338 24.9196 20.3938 24.9196C21.9538 24.9196 23.2402 23.6895 23.2402 22.1978C23.2134 20.7069 21.9538 19.476 20.3938 19.476ZM20.3938 23.2715C19.764 23.2715 19.2711 22.8001 19.2711 22.1979C19.2711 21.5956 19.764 21.1242 20.3938 21.1242C21.0236 21.1242 21.5166 21.5956 21.5166 22.1979C21.5166 22.7746 20.9959 23.2715 20.3938 23.2715Z" fill="white"/>
                                </svg> Comprar</a>
                        </div>
                    
                </div>
            </div>
            
        </div>
    </section>

    <section>
        <div className='container-prof' >
            <h2 className='cont-h2'> <span className='prof'> Profesionales</span> 
                <span className='spec'>Especialistas</span></h2>
            <div className="container-esp">
                <img className='uno' src={especialistas1}/>
                <img className='dos'src={especialistas2}/>
                <img className='tres'src={especialistas3}/>
                <img className='cuatro' src={especialistas4}/>
            </div>
        </div>
    </section>

    <section>
        <div>
            <div className="container-news">
                <div className='container-sub'>
                    <h3>Subscribe to Newsletters</h3>
                    <p>Be aware of upcoming sales and events. Receive gifts and special offers!</p>
                </div>

                <div className='container-input'>
                    <input type="text" placeholder="Ingresa tu Correo"/>
                    <button type="submit"> Subscribe
                    </button>
                </div>
            </div>
        </div>
    </section>
    </div>
  );
}

export default App;
